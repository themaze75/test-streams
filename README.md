# JMH Benchmark

## Build

`mvn clean install`

## Run benchmark

`java -jar target/benchmarks.jar`

NB: you can specify benchmarks to run (as regex)

ex: `java -jar target/benchmarks.jar Factorial`

## Show command-line help
`java -jar target/benchmarks.jar -h`

(highlights:)

* -r <time>  
  Time to spend at each measurement iteration.
* -rf <type>  
  Result format type. See the list of available result formats first.
* -lrf  
  Available formats: text, csv, scsv, json

`java -jar target/benchmarks.jar -rf csv -rff results.csv`

### JMH Benchmark Modes

* Throughput  
  Measures the number of operations per second, meaning the number of times per second your benchmark method could be executed.
* Average Time  
  Measures the average time it takes for the benchmark method to execute (a single execution).
* Sample Time  
  Measures how long time it takes for the benchmark method to execute, including max, min time etc.
* Single Shot Time  
  Measures how long time a single benchmark method execution takes to run. This is good to test how it performs under a cold start (no JVM warm up).
* All  
  Measures all of the above.

The default benchmark mode is **Throughput.**