// Copyright (c) MEI Computer Technology Group Inc, 2008

package com.maziade;

import static org.junit.Assert.assertEquals;
import java.util.List;
import org.junit.Test;

public class BenchSortedDistinctMediumTest
{
	@Test
	public void test_expectedResult()
	{
		final List<SomeObject> list = SomeObject.list(2000);

		final List<SomeObject> f1 = BenchSortedDistinctMedium.f1(list);

		assertEquals("f2", f1, BenchSortedDistinctMedium.f1_sortFirst(list));
		assertEquals("f2", f1, BenchSortedDistinctMedium.f2(list));
		assertEquals("f3_distinctFirst", f1, BenchSortedDistinctMedium.f3(list));
	}
}
