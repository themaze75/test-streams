// Copyright (c) MEI Computer Technology Group Inc, 2008

package com.maziade;

import static org.junit.Assert.assertEquals;
import java.math.BigInteger;
import java.util.stream.Collectors;
import java.util.stream.LongStream;
import org.junit.Test;


public class BenchFactorialTest
{
	@Test
	public void test_expectedResult()
	{
		LongStream
			.iterate(1,  val -> val + 1)
			.limit(10)
			.mapToObj(BigInteger::valueOf)
			.collect(Collectors.toList())
			.forEach(number -> {

				final BigInteger f1 = BenchFactorial.inefficientFactorial(number);

				assertEquals("f2", f1, BenchFactorial.hardToReadStream(number));
				assertEquals("f4", f1, BenchFactorial.readableStream(number));
			});
	}
}
