package com.maziade;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.HashSet;
import java.util.List;
import java.util.stream.Collectors;

import org.openjdk.jmh.annotations.Benchmark;

public class BenchSortedDistinctLarge
{
	final static List<SomeObject> baseLine = SomeObject.list(20_000);

	/**
	 * 
	 * @param list
	 * @return
	 */
	private static List<SomeObject> f1(List<SomeObject> list)
	{
		List<SomeObject> result = new ArrayList<SomeObject>(new HashSet<>(list));

		result.sort(Comparator.comparing(SomeObject::getDate).reversed());
		return result;
	}

	/**
	 * 
	 * @param list
	 * @return
	 */
	private static List<SomeObject> f2(List<SomeObject> list)
	{
		return list.stream()
				 .sorted(Comparator.comparing(SomeObject::getDate).reversed())
				 .distinct()
				 .collect(Collectors.toList());
	}
	
	/**
	 * 
	 * @param list
	 * @return
	 */
	private static List<SomeObject> f3(List<SomeObject> list)
	{
		return list.parallelStream()
				 .sorted(Comparator.comparing(SomeObject::getDate).reversed())
				 .distinct()
				 .collect(Collectors.toList());
	}

	@Benchmark
	public List<SomeObject> loop()
	{
		return f1(baseLine);
	}

	@Benchmark
	public List<SomeObject> stream()
	{
		return f2(baseLine);
	}
	
	@Benchmark
	public List<SomeObject> streamParallel()
	{
		return f3(baseLine);
	}
}
