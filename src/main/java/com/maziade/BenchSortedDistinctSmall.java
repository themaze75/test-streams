package com.maziade;

import java.util.*;
import java.util.stream.Collectors;
import org.openjdk.jmh.annotations.Benchmark;

public class BenchSortedDistinctSmall
{
	final static List<SomeObject> baseLine = SomeObject.list(500);

	/**
	 * 
	 * @param list
	 * @return
	 */
	private static List<SomeObject> f1(List<SomeObject> list)
	{
		List<SomeObject> result = new ArrayList<SomeObject>(new HashSet<>(list));

		result.sort(Comparator.comparing(SomeObject::getDate).reversed());
		return result;
	}

	/**
	 * 
	 * @param list
	 * @return
	 */
	private static List<SomeObject> f2(List<SomeObject> list)
	{
		return list.stream()
				 .sorted(Comparator.comparing(SomeObject::getDate).reversed())
				 .distinct()
				 .collect(Collectors.toList());
	}

	@Benchmark
	public List<SomeObject> loop()
	{
		return f1(baseLine);
	}

	@Benchmark
	public List<SomeObject> stream()
	{
		return f2(baseLine);
	}	
}
