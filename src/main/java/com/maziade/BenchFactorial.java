package com.maziade;

import java.math.BigInteger;
import java.util.stream.Stream;

import org.openjdk.jmh.annotations.Benchmark;

/**
 * Basis is not an optimal operation.  Idea here is to have similar integration but have one go through streams to see about the overhead
 * 
 * http://blog.takipi.com/benchmark-how-java-8-lambdas-and-streams-can-make-your-code-5-times-slower/
 * https://jaxenter.com/java-performance-tutorial-how-fast-are-the-java-8-streams-118830.html 
 */
public class BenchFactorial
{
	final static BigInteger commonTestParam = BigInteger.valueOf(6);
	
	// ===============================================================================================================================
	public static BigInteger inefficientFactorial(BigInteger number)
	{
		BigInteger cursor = BigInteger.ONE;
		BigInteger cumul = BigInteger.ONE;

		while(cursor.compareTo(number) < 0)
		{
			cursor = cursor.add(BigInteger.ONE);
			cumul = cumul.multiply(cursor);
		}

		return cumul;
	}

	// ===============================================================================================================================
	static final class Pair
	{
		BigInteger	cursor;
		BigInteger	cumul;

		Pair(BigInteger n, BigInteger v)
		{
			cursor = n;
			cumul = v;
		}

		Pair(Pair in)
		{
			cursor = in.cursor.add(BigInteger.ONE);
			cumul = in.cumul.multiply(cursor);
		}
		
		// this is breaking idempotence, but should be faster...
		static Pair step(Pair in)
		{
			in.cursor = in.cursor.add(BigInteger.ONE);
			in.cumul = in.cumul.multiply(in.cursor);
			return in;
		}
	}

	public static BigInteger hardToReadStream(BigInteger number)
	{
		return Stream
			.iterate(new Pair(BigInteger.ONE, BigInteger.ONE), Pair::new)
			.filter(x -> x.cursor.equals(number))
			.findFirst()
			.get().cumul;
	}

	// ===============================================================================================================================
	public static BigInteger readableStream(BigInteger number)
	{
		final BigInteger one = BigInteger.valueOf(1);

		return Stream
				.iterate(one, val -> val.add(one))
				.limit(number.longValue())
				.reduce(one, (cumul, x) -> cumul.multiply(x));
	}

	/**
	 * Iterative
	 */
	@Benchmark
	public void loop()
	{
		inefficientFactorial(commonTestParam);
	}
	
	/**
	 * Stream-based implementation
	 */
	@Benchmark
	public void stream()
	{
		hardToReadStream(commonTestParam);
	}

	/**
	 * Proper stream-based implementation
	 */
	@Benchmark
	public void streamProper()
	{
		readableStream(commonTestParam);
	}
}
