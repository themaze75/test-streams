package com.maziade;

import java.util.List;
import java.util.Random;
import java.util.stream.Collectors;
import org.openjdk.jmh.annotations.Benchmark;

/**
 * http://blog.takipi.com/benchmark-how-java-8-lambdas-and-streams-can-make-your-code-5-times-slower/
 * https://jaxenter.com/java-performance-tutorial-how-fast-are-the-java-8-streams-118830.html
 *
 */
public class BenchSimpleOpReduce
{
	final static List<Integer> commonTestParam = bigList(20_000);
	
	/**
	 * @param num
	 * @return
	 */
	private static List<Integer> bigList(long num)
	{
		return new Random()
			.ints()
			.limit(num)
			.mapToObj(Integer::valueOf)
			.collect(Collectors.toList());
	}

	/**
	 * 
	 * @param list
	 * @return
	 */
	private static int f1(List<Integer> list)
	{
		int result = 0;
		for(Integer val : list)
		{
			if (val.intValue() % 7 == 0)
				result += val.intValue() * 2;
		}
		
		return result;
	}
	
	/**
	 * 
	 * @param list
	 * @return
	 */
	private static int f2(List<Integer> list)
	{
		return list.stream().
			filter(val -> val.intValue() % 7 == 0).
			mapToInt(val -> val.intValue() * 2).
			reduce(0, Integer::sum);
	}

	// ===============================================================================================================================

	@Benchmark
	public void loop()
	{
		f1(commonTestParam);
	}

	@Benchmark
	public void stream()
	{
		f2(commonTestParam);
	}
}
