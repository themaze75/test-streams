package com.maziade;

import java.util.*;
import java.util.stream.Collectors;
import org.openjdk.jmh.annotations.Benchmark;

/**
 *
 */
public class BenchSortedDistinctMedium
{
	final static List<SomeObject> limitedCollisions = SomeObject.list(2000);
	final static List<SomeObject> multipleCollisions = SomeObject.list(2000, 20);

	/**
	 * 
	 * @param list
	 * @return
	 */
	public static List<SomeObject> f1(List<SomeObject> list)
	{
		List<SomeObject> result = new ArrayList<SomeObject>(new HashSet<>(list));

		result.sort(Comparator.comparing(SomeObject::getDate).reversed());
		return result;
	}
	
	/**
	 * 
	 * @param list
	 * @return
	 */
	public static List<SomeObject> f1_sortFirst(List<SomeObject> list)
	{
		List<SomeObject> result = new ArrayList<SomeObject>(list);
		result.sort(Comparator.comparing(SomeObject::getDate).reversed());

		return new ArrayList<>(new LinkedHashSet<>(result));
	}

	/**
	 * 
	 * @param list
	 * @return
	 */
	public static List<SomeObject> f2(List<SomeObject> list)
	{
		return list.stream()
				 .sorted(Comparator.comparing(SomeObject::getDate).reversed())
				 .distinct()
				 .collect(Collectors.toList());
	}
	
	/**
	 * 
	 * @param list
	 * @return
	 */
	public static List<SomeObject> f3(List<SomeObject> list)
	{
		return list.stream()
				 .distinct()
				 .sorted(Comparator.comparing(SomeObject::getDate).reversed())
				 .collect(Collectors.toList());
	}

	@Benchmark
	public List<SomeObject> loopLimitedCollisions()
	{
		return f1_sortFirst(limitedCollisions);
	}

	@Benchmark
	public List<SomeObject> loopLimitedCollisionsDistinctFirst()
	{
		return f1(limitedCollisions);
	}

	@Benchmark
	public List<SomeObject> streamLimitedCollisions()
	{
		return f2(limitedCollisions);
	}
	
	@Benchmark
	public List<SomeObject> streamLimitedCollisionsDistinctFirst()
	{
		return f3(limitedCollisions);
	}
	
	@Benchmark
	public List<SomeObject> loopMultipleCollisions()
	{
		return f1_sortFirst(multipleCollisions);
	}

	@Benchmark
	public List<SomeObject> loopMultipleCollisionsDistinctFirst()
	{
		return f1(multipleCollisions);
	}

	@Benchmark
	public List<SomeObject> streamMultipleCollisions()
	{
		return f2(multipleCollisions);
	}
	
	@Benchmark
	public List<SomeObject> streamMultipleCollisionsDistinctFirst()
	{
		return f3(multipleCollisions);
	}
}
