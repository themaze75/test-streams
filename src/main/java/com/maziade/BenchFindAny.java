package com.maziade;

import java.util.List;
import java.util.Random;
import java.util.stream.Collectors;
import java.util.stream.LongStream;
import org.openjdk.jmh.annotations.Benchmark;

/**
 * https://zeroturnaround.com/rebellabs/streams-the-real-powerhouse-in-java-8-by-venkat-subramaniam/
 *
 * A stream represents a pipeline through which the data will flow and the functions to operate on the data.
 */
public class BenchFindAny
{
	final static List<String> commonTestParam = bigList(20_000);
	final static String lookingFor = commonTestParam.get(commonTestParam.size() - 100).toLowerCase();

	/**
	 * @param num
	 * @return
	 */
	private static List<String> bigList(long num)
	{
		final Random rnd = new Random();

		return LongStream
			.iterate(0, val -> val + 1)
			.mapToObj(i -> String.format("%d-%08X", Long.valueOf(i), Long.valueOf(rnd.nextLong())))
			.limit(num)
			.collect(Collectors.toList());
	}

	// ===============================================================================================================================
	private static boolean f1(List<String> list, String lookFor)
	{
		for(String val : list)
		{
			val = val.toLowerCase();
			if (val.contains(lookFor))
				return true;
		}
		
		return false;
	}

	// ===============================================================================================================================
	private static boolean f2(List<String> list, String lookFor)
	{
		return list.stream().map(String::toLowerCase).anyMatch(lookFor::contains);
	}

	// ===============================================================================================================================
	private static boolean f3(List<String> list, String lookFor)
	{
		return list.parallelStream().map(String::toLowerCase).anyMatch(lookFor::contains);
	}

	// ===============================================================================================================================
	/**
	 * Iterative
	 */
	@Benchmark
	public void loop()
	{
		f1(commonTestParam, lookingFor);
	}
	
	/**
	 * Stream-based implementation
	 */
	@Benchmark
	public void stream()
	{
		f2(commonTestParam, lookingFor);
	}
	
	/**
	 * Non-stream, with algorithm more comparable to the stream
	 */
	@Benchmark
	public void streamParallel()
	{
		f3(commonTestParam, lookingFor);
	}
}
