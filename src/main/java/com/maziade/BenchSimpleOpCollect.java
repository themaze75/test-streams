package com.maziade;

import java.util.*;
import java.util.stream.Collectors;
import org.openjdk.jmh.annotations.Benchmark;

/**
 * https://zeroturnaround.com/rebellabs/streams-the-real-powerhouse-in-java-8-by-venkat-subramaniam/
 *
 * A stream represents a pipeline through which the data will flow and the functions to operate on the data.
 */
public class BenchSimpleOpCollect
{
	final static List<Integer> commonTestParam = bigList(20_000);

	/**
	 * @param num
	 * @return
	 */
	private static List<Integer> bigList(long num)
	{
		return new Random()
			.ints()
			.limit(num)
			.mapToObj(Integer::valueOf)
			.collect(Collectors.toList());
	}

	/**
	 * 
	 * @param list
	 * @return
	 */
	private static List<Integer> f1(List<Integer> list)
	{
		List<Integer> result = new ArrayList<>();

		for(Integer val : list)
		{
			if (val.intValue() % 2 == 0)
				result.add(Integer.valueOf(val.intValue() * 2));
		}
		
		return result;
	}
	
	/**
	 * 
	 * @param list
	 * @return
	 */
	private static List<Integer> f2(List<Integer> list)
	{
		return list.stream()
				.filter(e -> (e.intValue() % 2) == 0)
				.map(e -> Integer.valueOf(e.intValue() * 2))
				.collect(Collectors.toList());
	}

	// ===============================================================================================================================

	@Benchmark
	public void loop()
	{
		f1(commonTestParam);
	}

	@Benchmark
	public void stream()
	{
		f2(commonTestParam);
	}
}
