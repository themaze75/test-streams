// Copyright (c) MEI Computer Technology Group Inc, 2008

package com.maziade;

import java.util.*;
import java.util.function.LongFunction;
import java.util.stream.Collectors;

public class SomeObject
{
	private Date date;

	/**
	 * @param num
	 * @return
	 */
	public static List<SomeObject> list(long num)
	{
		return list(num, Optional.empty());
	}

	/**
	 * @param num
	 * @return
	 */
	public static List<SomeObject> list(long num, long range)
	{
		return list(num, Optional.of(Long.valueOf(range)));
	}

	/**
	 * @param num
	 * @return
	 */
	public static List<SomeObject> list(long num, Optional<Long> optRange)
	{
		final long range = optRange.orElse(Long.valueOf(0)).longValue();

		final LongFunction<Date> dateMapper = optRange.isPresent() ?
				val -> new Date(val % range) :
				val -> new Date(val);

		return new Random()
			.longs()
			.limit(num)
			.mapToObj(dateMapper)
			.map(SomeObject::new)
			.collect(Collectors.toList());
	}

	public SomeObject(Date date)
	{
		this.date = date;
	}
	
	public Date getDate()
	{
		return date;
	}

	@Override
	public boolean equals(Object obj)
	{
		return date.equals(((SomeObject)obj).date);
	}
}
